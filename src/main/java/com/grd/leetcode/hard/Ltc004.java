package com.grd.leetcode.hard;

/**
 * There are two sorted arrays nums1 and nums2 of size m and n respectively.
 * Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
 * You may assume nums1 and nums2 cannot be both empty.
 *
 * Example 1:
 *  nums1 = [1, 3]
 *  nums2 = [2]
 *  The median is 2.0
 *
 * Example 2:
 *  nums1 = [1, 2]
 *  nums2 = [3, 4]
 *  The median is (2 + 3)/2 = 2.5
 */
public class Ltc004 {

    public static void main(String[] arg) {
        int[] num1 = {1, 3};
        int[] num2 = {2};
        System.out.println(findMedianSortedArrays(num1, num2));

        int[] num3 = {1, 2};
        int[] num4 = {3, 4};
        System.out.println(findMedianSortedArrays(num3, num4));
    }

    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {

        int nums1key = 0;
        int nums2Key = 0;
        int index = 0;
        int[] merge= new int[nums1.length+nums2.length];

        while(nums1key < nums1.length || nums2Key < nums2.length) {

            if(nums1key==nums1.length) {
                merge[index] = nums2[nums2Key];
                nums2Key ++;
            } else if(nums2Key==nums2.length) {
                merge[index] = nums1[nums1key];
                nums1key ++;
            } else {
                if(nums1[nums1key]<=nums2[nums2Key]) {
                    merge[index] = nums1[nums1key];
                    nums1key++;
                } else {
                    merge[index] = nums2[nums2Key];
                    nums2Key ++;
                }
            }
            index ++;

        }

        return (index%2==0)?Double.valueOf(merge[(index/2)-1]+merge[(index/2)])/2:merge[(index/2)];
    }
}
