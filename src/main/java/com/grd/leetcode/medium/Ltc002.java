package com.grd.leetcode.medium;

/**
 * You are given two non-empty linked lists representing two non-negative integers.
 * The digits are stored in reverse order and each of their nodes contain a single digit.
 * Add the two numbers and return it as a linked list.
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 *
 * Example:
 *  Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 *  Output: 7 -> 0 -> 8
 *  Explanation: 342 + 465 = 807.
 *
 */
public class Ltc002 {

    public static void main(String[] arg) {
        ListNode a1 = new ListNode(2);
        ListNode a2 = new ListNode(4);
        ListNode a3 = new ListNode(3);
        a1.next = a2;
        a2.next = a3;
        ListNode a = a1;

        ListNode b1 = new ListNode(5);
        ListNode b2 = new ListNode(6);
        ListNode b3 = new ListNode(4);
        b1.next = b2;
        b2.next = b3;
        ListNode b = b1;

        addTwoNumbers(a, b);
    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode tmp1 = l1;
        ListNode tmp2 = l2;
        ListNode result = null;
        ListNode tmpListNode = null;
        int div = 0;
        while (tmp1!=null || tmp2!=null || div!=0) {

            int t = ((tmp1==null)?0:tmp1.val) + ((tmp2==null)?0:tmp2.val);
            ListNode tNode = new ListNode((t+div)%10);

            if(tmpListNode!=null) {
                tmpListNode.next = tNode;
            }
            tmpListNode = tNode;

            if(result==null) {
                result = tmpListNode;
            }
            div = (t+ div)/10;
            tmp1 = (tmp1==null)?null:tmp1.next;
            tmp2 = (tmp2==null)?null:tmp2.next;
        }
        return result;
    }
}

class ListNode {
    int val;
    ListNode next;
    ListNode(int x) {
        val = x;
    }
}
