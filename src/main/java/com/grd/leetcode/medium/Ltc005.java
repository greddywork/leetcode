package com.grd.leetcode.medium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.
 *
 * Example 1:
 *  Input: "babad"
 *  Output: "bab"
 *  Note: "aba" is also a valid answer.
 *
 * Example 2:
 *  Input: "cbbd"
 *  Output: "bb"
 */
public class Ltc005 {

    public static void main(String[] args) {
        System.out.println(longestPalindrome("babad"));
        System.out.println(longestPalindrome("cbbd"));
        System.out.println(longestPalindrome("a"));
        System.out.println(longestPalindrome("lejyqjcpluiggwlmnumqpxljlcwdsirzwlygexejhvojztcztectzrepsvwssiixfmpbzshpilmojehqyqpzdylxptsbvkgatzdlzphohntysrbrcdgeaiypmaaqilthipjbckkfbxtkreohabrjpmelxidlwdajmkndsdbbaypcemrwlhwbwaljacijjmsaqembgtdcskejplifnuztlmvasbqcyzmvczpkimpbbwxdtviptzaenkbddaauyvqppagvqfpednnckooxzcpuudckakutqyknuqrxjgfdtsxsoztjkqvfvelrklforpjnrbvyyvxigjhkjmxcphjzzilvbjbvwiwnnkbmboiqamgoimujtswdqesighoxsprhnsceshotakvmoxqkqjvbpqucvafiuqwmrlfjpjijbctfupywkbawquchbclgvhxbanybret"));
    }

    public static String longestPalindrome(String s) {

        String ans="";
        for(int i=1; i<=s.length(); i++) {
            if(i>ans.length()+2) {
                break;
            }
            for(int j=0; i+j<=s.length(); j++) {
                if(palindrome(s.substring(j, i+j))) {
                    ans = s.substring(j, i+j);
                    break;
                }
            }
        }

        return ans;
    }

    private static boolean palindrome(String s) {
        char[] chars = s.toCharArray();
        boolean check = true;
        for(int i=0; i<(chars.length/2)+1; i++) {
            if(chars[i] != chars[chars.length-i-1]) {
                check = false;
                break;
            }
        }
        return check;
    }


}
