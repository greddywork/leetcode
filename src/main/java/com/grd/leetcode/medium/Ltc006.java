package com.grd.leetcode.medium;

import java.util.ArrayList;
import java.util.List;

/**
 * The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this:
 * (you may want to display this pattern in a fixed font for better legibility)
 *
 * P   A   H   N
 * A P L S I I G
 * Y   I   R
 *
 * And then read line by line: "PAHNAPLSIIGYIR"
 * Write the code that will take a string and make this conversion given a number of rows:
 *  string convert(string s, int numRows);
 *
 * Example 1:
 *  Input: s = "PAYPALISHIRING", numRows = 3
 *  Output: "PAHNAPLSIIGYIR"
 *
 * Example 2:
 *
 *  Input: s = "PAYPALISHIRING", numRows = 4
 *  Output: "PINALSIGYAHRPI"
 *  Explanation:
 *
 *  P     I    N
 *  A   L S  I G
 *  Y A   H R
 *  P     I
 *
 */
public class Ltc006 {

    public static void main(String[] args) {
        System.out.println(convert("PAYPALISHIRING", 3));
        System.out.println(convert("PAYPALISHIRING", 4));
        System.out.println(convert("A", 1));
        System.out.println(convert("AB", 1));
        System.out.println(convert("ABC", 2));
    }

    public static String convert(String s, int numRows) {

        if(numRows==1) {
            return s;
        }

        List<StringBuffer> stringBufferList = new ArrayList<>();
        for(int i=0; i<numRows; i++) {
            stringBufferList.add(new StringBuffer());
        }

        int index = 0;
        for(char c:s.toCharArray()) {
            int x = index/numRows;
            int y = index%numRows;
            int jump = ((numRows-2)>=1)?numRows-2:0;
            stringBufferList.get(y).append(c);

            if(numRows-1>0 && x%(numRows-1)==0) {
                if((y+1)%numRows==0) {
                    index = index + jump;
                }
            } else {
                index = index + jump;
            }
            index++;
        }
        StringBuffer sb = new StringBuffer();
        for(StringBuffer stringBuffer:stringBufferList) {
            sb.append(stringBuffer);
        }
        return sb.toString();
    }
}
