package com.grd.leetcode.medium;

import java.util.*;

/**
 *
 * Given a string, find the length of the longest substring without repeating characters.
 *
 * Example 1:
 *  Input: "abcabcbb"
 *  Output: 3
 *  Explanation: The answer is "abc", with the length of 3.
 *
 * Example 2:
 *  Input: "bbbbb"
 *  Output: 1
 *  Explanation: The answer is "b", with the length of 1.
 *
 * Example 3:
 *  Input: "pwwkew"
 *  Output: 3
 *  Explanation: The answer is "wke", with the length of 3.
 *               Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 *
 */
public class Ltc003 {

    public static void main(String[] args) {
        System.out.println(lengthOfLongestSubstring("abcabcbb"));
        System.out.println(lengthOfLongestSubstring("bbbbb"));
        System.out.println(lengthOfLongestSubstring("pwwkew"));
        System.out.println(lengthOfLongestSubstring(" "));
    }

    public static int lengthOfLongestSubstring(String s) {

        Set<Byte> byteSet = new HashSet<>();
        int max=0;
        byte[] sb = s.getBytes();

        for(int i=0; i<sb.length; i++) {
            for(int j=i; j<sb.length; j++) {
                if(!byteSet.contains(sb[j])) {
                    byteSet.add(sb[j]);
                } else {
                    break;
                }
            }
            if(byteSet.size()>max) {
                max = byteSet.size();
            }
            byteSet = new HashSet<>();
        }

        return max;
    }
}
